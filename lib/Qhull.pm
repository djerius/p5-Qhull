package Qhull;

# ABSTRACT: Interface to the Qhull convex hull, Delauny triangulation, Voronoi diagram software suite

use v5.26;
use strict;
use warnings;

our $VERSION = '0.08';

sub import {
    my ( undef, @args ) = @_;
    # for now, just have PP version;
    require Qhull::PP;
    Qhull::PP->import( { into => scalar caller() }, @args );
}

1;

# COPYRIGHT

__END__

=for stopwords
qhull
Delauny
Voronoi

=head1 SYNOPSIS

   use Qhull 'qhull';

   # generate a convex hull and return the ordered
   # indices of the points in the convex hull
   my \@indices = qhull( $x, $y );

=head1 DESCRIPTION

This is an B<alpha> quality interface to the L<qhull|https://qhull.org> library and executables.

At present this module punts to L<Qhull::PP>, which is a wrapper
around B<qhull> executable, not the library.

At present see L<Qhull::PP> for a discussion of the arguments to L<qhull>.

=head2 Future API

B<qhull> has an interesting manner of setting up options, used by both
the executable and the library entry point.  It may be impossible to
get this to look Perlish, especially as the B<qhull> manual page is
required to properly use its facilities.

The final interface, which will be the same for the library and the
executable wrapper is still in flux.

=cut
